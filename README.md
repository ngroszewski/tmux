# tmux

Installs and configure tmux

## Role Variables

### tmux_g_opt

* Type: Dictionary
* Usage: Adds `set-option -g <key> <value>` in ~/.tmux.conf
* Example:

```
tmux_g_opt:
  status-bg: black
  status-fg: blue
```

### tmux_g_win_opt

* Type: Dictionary
* Usage: Adds `set-window-option -g <key> <value>` in ~/.tmux.conf
* Example:

```
tmux_g_win_opt:
  window-status-current-fg: white
```

### tmux_sg_opt

* Type: Dictionary
* Usage: Adds `set-window-option -sg <key> <value>` in ~/.tmux.conf
* Example:

```
tmux_sg_opt:
  escapte-time: 0
```

### tmux_unbinds

* Type: List
* Usage: Adds unbind config in ~/.tmux.conf
* Example:

```
tmux_unbinds:
  - "unbind-key C-b"
```

### tmux_binds

* Type: List
* Usage: Adds bind config in ~/.tmux.conf
* Example:

```
tmux_binds:
  - "bind-key 'c' new-window -c \"#{pane_current_path}\""
```

## Example Playbook

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - polkhan.tmux

## License

MIT
